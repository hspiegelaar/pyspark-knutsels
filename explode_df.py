def explode_aggregate(df):
    '''
    splits kolommen met '20.*' in jaar en weeknummer, concat voor alle kolommen 
    het sub-dataframe, exploderr over een datakolom
    '''

    df_result = None
    for column in df.columns:
        if column.startswith('20'):
            df_col = df.select('postcode', column) \
                .withColumn('year', year_from_datestring(lit(column))) \
                .withColumn('week', week_from_datestring(lit(column))) \
                .withColumnRenamed(column, 'token_string')

            if df_result:
                df_result = df_result.union(df_col)

            else:
                df_result = df_col

    df_result = df_result.withColumn(
        'token_string',
        split(df_result.token_string, ' ').cast(ArrayType(StringType())))

    df_result = df_result \
        .withColumn('token_aantal', explode(df_result.token_string)) \
        .drop('token_string')

    df_result = df_result \
        .withColumn('token', regexp_replace(df_result.token_aantal, ':.*', '')) \
        .withColumn('aantal', regexp_replace(df_result.token_aantal, '.*:', '')) \
        .drop('token_aantal')

    return df_result
