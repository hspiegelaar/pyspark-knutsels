from pyspark import SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf, col, lit, concat

import datetime as dt

sc = SparkContext("local", "First App")
spark = SparkSession.builder.getOrCreate()


@udf
def last_day_of_week(datestring):
    date = dt.datetime.strptime(datestring, '%Y%m%d')
    last_day = date - dt.timedelta(days=(date.weekday()-6))
    return dt.datetime.strftime(last_day, '%Y%m%d')


# -- inlezen en pre-processen
sample = spark.read.csv('data/sample.csv', header=True, sep=';').fillna('')
sample = sample.withColumn('date', last_day_of_week(col('date')))

aggregate = spark.read.csv('data/aggregate.csv', header=True, sep=';').fillna('')

aggregate = aggregate.withColumn('serial_column', lit(''))

for this_column in aggregate.columns:

    if this_column.startswith('20'):
        aggregate = aggregate.withColumn(
            'tmp',
            concat(aggregate.serial_column, lit('|'), lit(this_column), lit(' '), col(this_column))).drop(this_column)

        aggregate = aggregate.drop('serial_column')
        aggregate = aggregate.withColumnRenamed('tmp', 'serial_column')

        aggregate.printSchema()

aggregate.show(10, False)

